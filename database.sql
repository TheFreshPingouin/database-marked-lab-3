drop table carTypes;
drop table class;
drop table cities;
drop table periods;
drop table trainLines;
drop table calendar;
drop table trains;
drop table stopsIn;
drop table rides;
drop table stopsAt;
drop table cityLinks;
drop table bookings;
drop table cars;
drop table seats;

create table carTypes (
	car_type				varchar(2),
	class					int references class(id),
	num_seat_min			int,
	num_seat_max			int,
	primary key (car_type)
);
create table class (
	id						int,
    kilometer_price			float,
    primary key (id)
);


create table cities (
	city_id					int NOT NULL AUTO_INCREMENT,
	city_name				varchar(30),
    primary key (city_id)
);


create table periods (
    period_color			varchar(10),
	discount_coeff			float,
    primary key (period_color)
);

create table trainLines (
	line_id					int,
    primary key (line_id)
);

create table calendar (
	begin_date				date not null,
    end_date				date not null,
	period_color			varchar(10) references periods(period_color),
    primary key (begin_date, end_date)
	
	 -- => check if period already exists

);

-- 
create table trains (
    train_number			int,
    line_id					int references trainLines(line_id),
	direction				boolean, -- true if we follow the stops order
    primary key (train_number)
	
	-- => check if line already exists
);

create table stopsIn (
	line_id					int references trainLines(line_id),
	city_id					varchar(30) references cities(city_id),
	stop_number				int,
	primary key (line_id, city_id)
	-- => checks if line and city already exists

);

create table rides (
	rid						int NOT NULL AUTO_INCREMENT, -- a mettre dans le schema	
    train_number			int references trains(train_number),
	period_color			varchar(10) references periods(period_color),
    primary key (rid)
	
	-- => checks if period already exists

);

create table stopsAt (
	rid						int references ride(rid),
	city_id					varchar(30) references cities(city_id),
	time_of_arrival			time,
	primary key (rid, city_id)
	
	-- => check if rides and cities already exists


);

create table cityLinks (
	
    city_id_one				varchar(30) references cities(city_id),
    city_id_two				varchar(30) references cities(city_id),
	distance				int,
    primary key (city_id_one, city_id_two)
	
	-- => checks if cities already exist

);
create table bookings (
	booking_id				varchar(6),
    train_number			int references trains(train_number),
    client_email			varchar(255),
	departure_date_time		datetime,
    primary key (booking_id)
	
	-- => checks if train already exists

);

create table cars (
	car_number				int,
    car_type				varchar(2) references carTypes(car_type),
    number_of_seats			int,
	rid						int references ride(rid),
    primary key (car_number,rid)
	
	-- => checks if cartype already exists
);


create table seats (
	booking_id				varchar(6) references bookings(booking_id),
	rid						int references rides(rid),
    car_number				int references cars(car_id),
    seat_number				int,
	departure_city_id		varchar(30) references cities(city_id),
	arrival_city_id			varchar(30) references cities(city_id),
	seat_price				int,
    primary key (booking_id, car_number, seat_number, rid)
	
	-- => checks if car already exist
	-- => checks if seat_number between the range (num_seat_min,num_seat_max) of car.carType AND if seat_number is not already taken

);



INSERT INTO carTypes(car_type,class,num_seat_min,num_seat_max) VALUES
('S1',1,11,46),
('S2',2,11,66),
('D1',1,11,70),
('D2',2,11,102),
('SB',2,21,36),
('DB',2,0,-1);


INSERT INTO class (id,kilometer_price) VALUES
(1,0.295),
(2,0.208);

INSERT INTO cities (city_name) VALUES
('Paris'),
('Lyon'),
('Avignon'),
('Marseille'),
('Strasbourg'),
('Montpellier');

INSERT INTO periods (period_color,discount_coeff) VALUES
('red',1.3),
('white',1.0),
('blue',0.8);

INSERT INTO trainLines (line_id) VALUES
(1),
(2);

INSERT INTO calendar (begin_date, end_date,period_color) VALUES
('2017-10-01','2017-10-01','white'),
('2017-10-02','2017-10-06','blue'),
('2017-10-07','2017-10-08','white'),
('2017-10-09','2017-10-13','blue'),
('2017-10-14','2017-10-15','white'),
('2017-10-16','2017-10-20','blue'),
('2017-10-21','2017-10-22','white'),
('2017-10-23','2017-10-27','blue'),
('2017-10-28','2017-10-31','red');

INSERT INTO trains (train_number,line_id,direction) VALUES
(6607,1,true),
(6650,2,true),
(6632,1,false);

INSERT INTO stopsIn (line_id,city_id,stop_number) VALUES
(1,1,1),
(1,2,2),
(1,3,3),
(1,4,4),

(2,5,1),
(2,2,2),
(2,3,3),
(2,6,4);

INSERT INTO rides (train_number,period_color) VALUES
(6607,'red'),
(6607,'white'),
(6607,'blue'),

(6650,'red'),

(6632,'white'),
(6632,'red');



INSERT INTO stopsAt (rid,city_id,time_of_arrival) VALUES
(1,1,'08:00:00'),
(1,2,'09:30:00'),
(1,3,'11:00:00'),
(1,4,'11:30:00'),

(2,1,'07:30:00'),
(2,2,'09:00:00'),
(2,3,'10:30:00'),
(2,4,'11:00:00'),

(3,1,'08:30:00'),
(3,2,'10:00:00'),
(3,3,'11:30:00'),
(3,4,'12:00:00'),

(4,5,'09:45:00'),
(4,2,'13:15:00'),
(4,3,'14:55:00'),
(4,6,'15:55:00'),

(5,4,'14:00:00'),
(5,3,'14:40:00'),
(5,2,'16:20:00'),
(5,1,'19:00:00'),

(6,4,'13:30:00'),
(6,3,'14:10:00'),
(6,2,'15:50:00'),
(6,1,'18:30:00');








INSERT INTO cityLinks (city_id_one,city_id_two,distance) VALUES
(1,2,465),
(2,3,230),
(3,4,105),
(3,5,95),
(5,2,495);


INSERT INTO bookings (booking_id,train_number,client_email,departure_date_time) VALUES
('ABCDEF',6607,'jane.smith@gmail.com','2017-10-29 10:00:00');

INSERT INTO cars (car_number,car_type,number_of_seats,rid) VALUES
(1,'S1',36,1),
(2,'S1',36,1),
(3,'S1',36,1),
(4,'SB',16,1),
(5,'S2',56,1),
(6,'S2',56,1),
(7,'S2',56,1),
(8,'S2',56,1),

(1,'D1',60,2),
(2,'D1',60,2),
(3,'D1',60,2),
(4,'DB',0,2),
(5,'D2',92,2),
(6,'D2',92,2),
(7,'D2',92,2),
(8,'D2',92,2),

(1,'D1',60,3),
(2,'D1',60,3),
(3,'D1',60,3),
(4,'DB',0,3),
(5,'D2',92,3),
(6,'D2',92,3),
(7,'D2',92,3),
(8,'D2',92,3),
(11,'D1',60,3),
(12,'D1',60,3),
(13,'D1',60,3),
(14,'DB',0,3),
(15,'D2',92,3),
(16,'D2',92,3),
(17,'D2',92,3),
(18,'D2',92,3),

(1,'D1',60,4),
(2,'D1',60,4),
(3,'D1',60,4),
(4,'DB',0,4),
(5,'D2',92,4),
(6,'D2',92,4),
(7,'D2',92,4),
(8,'D2',92,4),

(1,'S1',36,5),
(2,'S1',36,5),
(3,'S1',36,5),
(4,'SB',16,5),
(5,'S2',56,5),
(6,'S2',56,5),
(7,'S2',56,5),
(8,'S2',56,5),

(1,'D1',60,6),
(2,'D1',60,6),
(3,'D1',60,6),
(4,'DB',0,6),
(5,'D2',92,6),
(6,'D2',92,6),
(7,'D2',92,6),
(8,'D2',92,6),
(11,'D1',60,6),
(12,'D1',60,6),
(13,'D1',60,6),
(14,'DB',0,6),
(15,'D2',92,6),
(16,'D2',92,6),
(17,'D2',92,6),
(18,'D2',92,6);


INSERT INTO seats (booking_id,rid,car_number,seat_number,departure_city_id,arrival_city_id,seat_price) VALUES
('ABCDEF',2,2,21,2,3,20),
('ABCDEF',2,2,22,2,3,20),
('ABCDEF',2,2,23,2,3,20);

