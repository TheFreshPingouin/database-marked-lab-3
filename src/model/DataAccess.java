package model;

//import java.util.*;
import java.io.File;
import java.io.FileInputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/**
 * Provides the application with high-level methods to access the persistent
 * data store. The class hides the fact that data are stored in a RDBMS and also
 * hides all the complex SQL machinery required to access it.
 * <p>
 * The constructor and the methods of this class all throw a
 * {@link DataAccessException} whenever an unrecoverable error occurs, e.g. the
 * connexion to the database is lost.
 * <p>
 * <b>Note to the implementors</b>: You <b>must not</b> alter the interface of
 * this class' constructor and methods, including the exceptions thrown.
 *
 * @author Jean-Michel Busca
 */
public class DataAccess {

    /**
     * Creates a new <code>DataAccess</code> object that interacts with the
     * specified database, using the specified login and password. Each object
     * maintains a <b>dedicated</b> connection to the database until the
     * {@link close} method is called.
     *
     * @param url the url of the database to connect to
     * @param login the (application) login to use
     * @param password the password
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    
    private Connection connection;
    private PreparedStatement ps;
    
    
    public DataAccess(String url, String login, String password) throws DataAccessException, SQLException {
        
        connection = DriverManager.getConnection(url, login, password);
        System.out.println("OK!");
        connection.setAutoCommit(false);
    }

    /**
     * Creates and populates the database according to all the examples provided
     * in the requirements of marked lab 2. If the database already exists
     * before the method is called, the method discards the database and creates
     * it again from scratch.
     * <p>
     * This implementation executes the SQL script named
     * <code>database.sql</code> located in the project's root directory. It
     * assumes the <code>DataAccess</code> class declares the connection
     * attribute as follows:
     * <p>
     * <code>private Connection connection;</code>
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public void initDatabase() throws DataAccessException {
        
        int okCount = 0;
        try {
            // read file
            File file = new File("database.sql");
            FileInputStream stream = new FileInputStream(file);
            byte[] data = new byte[(int) file.length()];
            stream.read(data);
            stream.close();

            // split contents into statements
            String contents = new String(data);
            String statements[] = contents.split(";");

            // execute statements
            Statement jdbc = connection.createStatement();
            for (String statement : statements) {
                // remove comments
                statement = statement.replaceAll(" *-- .*(\\n|\\r)", "");
                // remove end of lines 
                statement = statement.replaceAll(" *(\\n|\\r)", "");
                if (statement.isEmpty()) {
                    continue;
                }
                String message = "initDatabase(): '" + statement + "': ";
                try {
                    jdbc.executeUpdate(statement);
                    System.out.println(message + "ok");
                    okCount += 1;
                } catch (SQLException e) {
                    System.err.println(message + "FAILED (" + e.getMessage() + ")");
                }
            }
        } catch (Exception e) {
            throw new DataAccessException(e);
        }
        if (okCount == 0) {
            throw new DataAccessException("failed to create database");
        }
        
    }

    /**
     * See Operation 2.1.1.
     *
     * @param departureStation
     * @param arrivalStation
     * @param fromDate
     * @param toDate
     *
     * @return the corresponding list of journeys, including the empty list if
     * no journey is found
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public List<Journey> getTrainTimes(String departureStation, String arrivalStation, Date fromDate, Date toDate)
        throws DataAccessException, SQLException {
        
        // Initialization
        List<Journey> list = new ArrayList<Journey>();
        
        // Let's get the first city id
        PreparedStatement stmt = null;
        stmt = connection.prepareStatement("SELECT city_id FROM cities WHERE city_name= '" + departureStation +"'");
        ResultSet result = stmt.executeQuery();
        result.next();
        int cityId1 = result.getInt(1);
        
        // Let's get the second city id
        stmt = connection.prepareStatement("SELECT city_id FROM cities WHERE city_name= '" + arrivalStation + "'");
        ResultSet result1 = stmt.executeQuery();
        result1.next();
        int cityId2 = result1.getInt(1);
        
        System.out.println("");
        System.out.println("model.DataAccess.getTrainTimes()");
        System.out.println("1st city id: " + cityId1);
        System.out.println("2nd city id: " + cityId2);
        
        // Now do the algoritm that finds all the line ids in which both cities belongs to
        stmt = connection.prepareStatement("SELECT line_id, city_id FROM stopsIn WHERE city_id= " + cityId1 + " or city_id="+ cityId2+ " order by line_id");
        ResultSet resultLine = stmt.executeQuery();
        int cityCount = 0;
        int lid = 0;
        int trainNumber = 0;
        Date depDate = new Date();
        Date arrDate = new Date();
        Date temp = new Date();
        Time depTime, arrTime;
        int currentDay = 0;
        Calendar cal = Calendar.getInstance();
        
        while(resultLine.next()){
            if(resultLine.getInt(1) != lid){
                cityCount = 0;
                lid = resultLine.getInt(1);
                System.out.println("");
                System.out.println("Line id " + lid + " is being checked...");
                System.out.println("");
            }
            if(resultLine.getInt(2) == cityId1 || resultLine.getInt(2) == cityId2){
                cityCount++;
                System.out.println("A city was found, the count is now: " + cityCount);
                
                if(cityCount == 2){
                    // For each line which has both stops on it
                    
                    // Stop number of city 1 on that line
                    stmt = connection.prepareStatement("SELECT stop_number FROM stopsIn where line_id="+lid+" and city_id="+cityId1);
                    ResultSet stopnum1 = stmt.executeQuery();
                    stopnum1.next();

                    // Stop number of city 2 on that line
                    stmt = connection.prepareStatement("SELECT stop_number FROM stopsIn where line_id="+lid+" and city_id="+cityId2);
                    ResultSet stopnum2 = stmt.executeQuery();
                    stopnum2.next();

                    System.out.println("");
                    System.out.println("Stop number of city 1: " + stopnum1.getInt(1));
                    System.out.println("Stop number of city 2: " + stopnum2.getInt(1));

                    //we can then deduce the direction
                    boolean direction;

                    if(stopnum1.getInt(1) < stopnum2.getInt(1))
                        direction = true;
                    else
                        direction = false;

                    System.out.println("Direction is " + direction);
                    System.out.println("");

                    // Now we get the train number using direction and line. 
                    stmt = connection.prepareStatement("SELECT train_number FROM trains WHERE line_id=" + lid+" and direction="+direction);
                    ResultSet resultTrains = stmt.executeQuery();
                    resultTrains.next();
                    trainNumber = resultTrains.getInt(1);
                    
                    // For each day between fromDate.getDay() and toDate.getDay()
                    
                    // Get first day
                    cal.setTime(fromDate);
                    
                    // Setting time to 00:00:00
                    cal.set(Calendar.HOUR_OF_DAY, 0);
                    cal.set(Calendar.MINUTE, 0);
                    cal.set(Calendar.SECOND, 0);
                    
                    temp.setTime(cal.getTimeInMillis());
                    
                    System.out.println("Calendar date: " + cal.getTime().toString());
                    currentDay = cal.DAY_OF_MONTH;
                    
                    do{
                        temp.setTime(cal.getTimeInMillis());
                        System.out.println("Day is " + currentDay);
                        
                        
                        // Obtaining period color with departure date
                        ps = connection.prepareStatement("SELECT period_color FROM calendar WHERE begin_date <= ? AND end_date >= ? ");
                        ps.setDate(1, new java.sql.Date(cal.getTimeInMillis()));
                        ps.setDate(2, new java.sql.Date(cal.getTimeInMillis()));
                        ResultSet period_color = ps.executeQuery();
                        period_color.next();
                        System.out.println("The period is " + period_color.getString(1));

                        // Obtaining ride id with period color and train number
                        ps = connection.prepareStatement("SELECT rid FROM rides WHERE train_number = " + trainNumber + " AND period_color = '" + period_color.getString(1) + "'");
                        ResultSet ride_id = ps.executeQuery();
                        
                        // If the ride exists
                        if(ride_id.next()){
                            System.out.println("Ride " + ride_id.getInt(1) + " is in train number " + trainNumber + " on the " + period_color.getString(1) + " period.");
                            System.out.println("");

                            // Obtaining the time of departure with the ride id and the city id of departure
                            ps = connection.prepareStatement("SELECT time_of_arrival FROM stopsAt WHERE rid = ? and city_id = ? ");
                            ps.setInt(1, ride_id.getInt(1));
                            ps.setInt(2, cityId1);
                            ResultSet dapartureTime = ps.executeQuery();
                            dapartureTime.next();
                            depTime = new Time(dapartureTime.getTime(1).getTime());
                            
                            cal.setTime(depTime);
                            int h = cal.get(Calendar.HOUR_OF_DAY);
                            int m = cal.get(Calendar.MINUTE);
                            int s = cal.get(Calendar.SECOND);
                            
                            cal.setTime(temp);
                            cal.add(Calendar.HOUR_OF_DAY, h);
                            cal.add(Calendar.MINUTE, m);
                            cal.add(Calendar.SECOND, s);
                            
                            depDate.setTime(cal.getTimeInMillis());
                            System.out.println("Date and Time of departure is " + depDate.toString());

                            // Obtaining the time of arrival with the ride id and the city id of departure
                            ps = connection.prepareStatement("SELECT time_of_arrival FROM stopsAt WHERE rid = ? and city_id = ? ");
                            ps.setInt(1, ride_id.getInt(1));
                            ps.setInt(2, cityId2);
                            ResultSet arrivalTime = ps.executeQuery();
                            arrivalTime.next();
                            arrTime = new Time(arrivalTime.getTime(1).getTime());
                            
                            cal.setTime(arrTime);
                            h = cal.get(Calendar.HOUR_OF_DAY);
                            m = cal.get(Calendar.MINUTE);
                            s = cal.get(Calendar.SECOND);
                            
                            cal.setTime(temp);
                            cal.add(Calendar.HOUR_OF_DAY, h);
                            cal.add(Calendar.MINUTE, m);
                            cal.add(Calendar.SECOND, s);
                            
                            arrDate.setTime(cal.getTimeInMillis());
                            System.out.println("Date and Time of arrival is " + arrDate.toString());
                            //arrivalTime.getTime(1);

                            // Check if the time of that ride is between fromDate and toDate
                            if(depDate.compareTo(fromDate) >= 0 && depDate.compareTo(toDate) <= 0){
                                // If yes, add the Journey to the list
                                Date a = new Date(depDate.getTime());
                                Date b = new Date(arrDate.getTime());
                                Journey j = new Journey(departureStation, arrivalStation, trainNumber, a, b);
                                list.add(j);
                            }
                        }
                        else{
                            System.out.println("There is no ride for that train on that period.");
                        }
                        
                        cal.setTime(temp);
                        cal.add(Calendar.DATE, 1);
                        currentDay = cal.DAY_OF_MONTH;
                    }while(cal.getTime().compareTo(fromDate) >= 0 && cal.getTime().compareTo(toDate) <= 0);
                }
            }
        }
        System.out.println("");
        if(!resultLine.next()){
            System.out.println("It was the last line.");
        }
        
        System.out.println("");
        System.out.println("The selected Journeys for your search are: ");
        
        for (Iterator<Journey> iterator = list.iterator(); iterator.hasNext();) {
            Journey next = iterator.next();
            System.out.print("- ");
            System.out.println(next.toString());
        }
        System.out.println("");
        
        
        return list;
    }

    /**
     * See Operation 2.1.2
     *
     * @param departureStation
     * @param arrivalStation
     * @param travelPeriod
     * @param passengerCount
     * @param travelClass
     *
     * @return the bought ticket, or <code>null</code> if some parameter was
     * incorrect
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public Ticket buyTicket(String departureStation, String arrivalStation, Period travelPeriod, int passengerCount, Class travelClass)
        throws DataAccessException, SQLException {
        
        // Various declarations
        PreparedStatement ps = null;
        PreparedStatement stmt = null;
        float amount = 0;
        float discount = 0;
        
        // Getting the discount
        ps = connection.prepareStatement("SELECT discount_coeff FROM periods WHERE period_color = ? ");
        ps.setString(1, travelPeriod.toString());
        ResultSet coef = ps.executeQuery();
        coef.next();
        discount = coef.getFloat(1);
        System.out.println("Discount coefficient: " + discount);
        
        
        
        //we get the first city id
        stmt = connection.prepareStatement("SELECT city_id FROM cities WHERE city_name= '" + departureStation +"'");
        ResultSet result = stmt.executeQuery();
        result.next();
        int cityId1 = result.getInt(1);
        
        //we get the second city id
        stmt = connection.prepareStatement("SELECT city_id FROM cities WHERE city_name= '" + arrivalStation + "'");
        ResultSet result1 = stmt.executeQuery();
        result1.next();
        int cityId2 = result1.getInt(1);
        
        //algoritm that finds the first line id in which both cities belongs to
        stmt = connection.prepareStatement("SELECT line_id,city_id FROM stopsIn WHERE city_id= " + cityId1 + " or city_id="+ cityId2+ " order by line_id");
        ResultSet resultLine = stmt.executeQuery();
        int cityCount = 0;
        int lid = 0;
        while(resultLine.next() || cityCount < 2){ 
            
            if(resultLine.getInt(1) != lid){
                cityCount = 0;
                lid = resultLine.getInt(1);
            }
            if(resultLine.getInt(2) == cityId1 || resultLine.getInt(2) == cityId2)
                cityCount++;
            
            
        }

        //we get the stop number of the city 1 on that line
        stmt = connection.prepareStatement("SELECT stop_number FROM stopsIn where line_id="+lid+" and city_id="+cityId1);
        ResultSet resultTemp = stmt.executeQuery();

        //we get the stop number of the city 2 on that line
        stmt = connection.prepareStatement("SELECT stop_number FROM stopsIn where line_id="+lid+" and city_id="+cityId2);
        ResultSet resultTemp2 = stmt.executeQuery();

        resultTemp.next();
        resultTemp2.next();

        //we can then deduce the direction
        boolean direction;
        if(resultTemp.getInt(1) < resultTemp2.getInt(1))
            direction = true;
        else
            direction = false;
        
        // we get the train number from that direction and that line. 
        stmt = connection.prepareStatement("SELECT train_number FROM trains WHERE line_id=" + lid+" and direction="+direction);
        ResultSet resultTrains = stmt.executeQuery();
        resultTrains.next();
        int trainNumber = resultTrains.getInt(1);
        
        
        
        // NOW WE NEED TO COMPUTE THE TOTAL PRICE
        ps = connection.prepareStatement("SELECT line_id FROM trains WHERE train_number = " + trainNumber);
        ResultSet lineId = ps.executeQuery();
        lineId.next();
        
        // Let's look inside stopsIn table
        ps = connection.prepareStatement("SELECT city_id, stop_number FROM stopsIn WHERE line_id = " + lineId.getInt(1) + " order by stop_number");
        ResultSet stops = ps.executeQuery();
        System.out.println("");
        
        // Printing city ids
        System.out.println("City of departure: " + cityId1);
        System.out.println("City of arrival: " + cityId2);
        System.out.println("");
        
        Boolean countKilometers = false;
        int km = 0;
        int prevCity = -1;
        while(stops.next()){
            if(countKilometers){
                // Find the distance between this a city and its previous city
                ps = connection.prepareStatement("SELECT distance FROM cityLinks WHERE (city_id_one = ? AND city_id_two = ?) OR (city_id_one = ? AND city_id_two = ?)");
                ps.setInt(1, prevCity);
                ps.setInt(2, stops.getInt(2));
                ps.setInt(3, stops.getInt(2));
                ps.setInt(4, prevCity);
                ResultSet cityLinks = ps.executeQuery();
                cityLinks.next();
                
                // We increment the total distance
                km += cityLinks.getInt(1);
                
                System.out.println("Distance between " + prevCity + " and " + stops.getInt(2) + ": " + cityLinks.getInt(1));
            }
            
            System.out.println("Stop " + stops.getInt(2) + " is city " + stops.getInt(1));
            
            // If it is departure or arrival, we start or stop counting
            if(stops.getInt(1) == cityId1 || stops.getInt(1) == cityId2){
                if(!countKilometers){
                    // Start
                    countKilometers = true;
                }
                else{
                    // Stop
                    countKilometers = false;
                }
            }
            
            
            prevCity = -1;
            if(countKilometers){
                // Between this city id and the next one
                prevCity = stops.getInt(1);
            }
        }
        
        // Total distance
        System.out.println("");
        System.out.println("The total distance for your trip is " + km);
        System.out.println("");
        
        // Calculating the class coefficient
        int classNumber = (travelClass == Class.FIRST) ? 1 : 2;
        ps = connection.prepareStatement("SELECT kilometer_price FROM class WHERE id = ?");
        ps.setInt(1, classNumber);
        ResultSet kilometer_price = ps.executeQuery();
        kilometer_price.next();
        float km_price = kilometer_price.getFloat(1);
        

        System.out.println("Your reservation is about to be completed...");
        System.out.println("");

        // Now computing the price
        amount = passengerCount * ((km * km_price) * discount);

        System.out.println("Cost for one person on this trip: " + (km * km_price));
        System.out.println("With the period discount coef(" + discount + "): " + discount * (km * km_price));
        System.out.println("Number of people: " + passengerCount);
        System.out.println("");
        System.out.println("Total cost of your trip is " + amount);

        // Sending the booking
        Ticket t = new Ticket(departureStation, arrivalStation, travelPeriod, passengerCount, travelClass, amount);
        return t;
    }

    /**
     * See Operation 2.1.3.
     *
     * @param trainNumber
     * @param departureDate
     * @param departureStation
     * @param arrivalStation
     * @param passengerCount
     * @param travelClass
     * @param customerEmail
     *
     * @return the booking, or <code>null</code> if some parameter was incorrect
     * or not enough seats were available
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public Booking buyTicketAndBook(int trainNumber, Date departureDate, String departureStation, String arrivalStation, int passengerCount, Class travelClass, String customerEmail)
        throws DataAccessException, SQLException {
        
        // Various declarations
        PreparedStatement ps = null;
        int nbOfAvailableSeats = 0;
        List<Seat> seatsList = new ArrayList<Seat>();
        float amount = 0;
        float discount = 0;
        
        // Obtaining period color with departure date
        ps = connection.prepareStatement("SELECT period_color FROM calendar WHERE begin_date <= ? AND end_date >= ? ");
        ps.setDate(1, new java.sql.Date(departureDate.getTime()));
        ps.setDate(2, new java.sql.Date(departureDate.getTime()));
        ResultSet period_color = ps.executeQuery();
        period_color.next();
        System.out.println(period_color.getString(1));
        
        // Getting the discount
        ps = connection.prepareStatement("SELECT discount_coeff FROM periods WHERE period_color = ? ");
        ps.setString(1, period_color.getString(1));
        ResultSet coef = ps.executeQuery();
        coef.next();
        discount = coef.getFloat(1);
        System.out.println("Discount coefficient: " + discount);
        
        // Obtaining ride id with period color and train number
        ps = connection.prepareStatement("SELECT rid FROM rides WHERE train_number = " + trainNumber + " AND period_color = '" + period_color.getString(1) + "'");
        ResultSet ride_id = ps.executeQuery();
        ride_id.next();
        System.out.println("Ride " + ride_id.getInt(1) + " is in train number " + trainNumber + " on the " + period_color.getString(1) + " period.");
        System.out.println("");
        
        
        // Obtaining Line id
        ps = connection.prepareStatement("SELECT line_id FROM trains WHERE train_number = " + trainNumber);
        ResultSet lineId = ps.executeQuery();
        lineId.next();
        
        // Now we can look inside stopsIn table
        ps = connection.prepareStatement("SELECT city_id, stop_number FROM stopsIn WHERE line_id = " + lineId.getInt(1) + " order by stop_number");
        ResultSet stops = ps.executeQuery();
        System.out.println("");
        
        // We get both ids of the departure and arrival cities
        ResultSet cityId;
        
        ps = connection.prepareStatement("SELECT city_id FROM cities WHERE city_name = '" + departureStation + "'");
        cityId = ps.executeQuery();
        cityId.next();
        int cityId1 = cityId.getInt(1);
        System.out.println("City of departure: " + cityId1);
        
        ps = connection.prepareStatement("SELECT city_id FROM cities WHERE city_name = '" + arrivalStation + "'");
        cityId = ps.executeQuery();
        cityId.next();
        int cityId2 = cityId.getInt(1);
        System.out.println("City of arrival: " + cityId2);
        System.out.println("");
        
        
        
        // For each car in a ride
        ps = connection.prepareStatement("SELECT * FROM cars WHERE rid = " + ride_id.getInt(1));
        ResultSet cars = ps.executeQuery();
        while(cars.next()){
            System.out.println("The car " + cars.getInt("car_number") + " is being checked...");
            
            // Nb of available seats set to nb of seats
            nbOfAvailableSeats = cars.getInt("number_of_seats");
            
            ps = connection.prepareStatement("SELECT * FROM carTypes WHERE car_type = '" + cars.getString("car_type") + "'");
            ResultSet carType = ps.executeQuery();
            carType.next();
            
            System.out.println("The car class is " + carType.getInt("class") + ".");
            
            if((carType.getInt("class") == 1 && travelClass == Class.FIRST) || (carType.getInt("class") == 2 && travelClass == Class.SECOND)){
                System.out.println("Class okay.");
                
                // Look for available seats in this car
                ps = connection.prepareStatement("SELECT seat_number, departure_city_id, arrival_city_id FROM seats WHERE car_number = " + cars.getInt("car_number") + " AND rid = " + ride_id.getInt(1));
                ResultSet seats = ps.executeQuery();
                
                // Flags array for seats
                List<Boolean> available = new ArrayList<Boolean>();
                for(int i = carType.getInt("num_seat_min"); i < carType.getInt("num_seat_max") + 1; i++){
                    available.add(Boolean.TRUE);
                }
                System.out.println("size of array is " + available.size());
                
                while(seats.next()){
                    // If it was not already put to false
                    if(available.get(seats.getInt(1) - carType.getInt("num_seat_min")) != Boolean.FALSE){
                        
                        // Check if the seat is reserved between city 1 and 2
                        // Getting stop numbers of both cities of the ticket from stopsIn
                        ps = connection.prepareStatement("SELECT stop_number FROM stopsIn WHERE (city_id = ? OR city_id = ?) AND line_id = ? ");
                        ps.setInt(1, seats.getInt(2));
                        ps.setInt(2, seats.getInt(3));
                        ps.setInt(3, lineId.getInt(1));
                        ResultSet a = ps.executeQuery();
                        a.next();
                        int first = a.getInt(1);
                        a.next();
                        int second = a.getInt(1);

                        // Getting stop numbers of both cities of the actual reservation from stopsIn
                        ps = connection.prepareStatement("SELECT stop_number FROM stopsIn WHERE (city_id = ? OR city_id = ?) AND line_id = ? ");
                        ps.setInt(1, cityId1);
                        ps.setInt(2, cityId2);
                        ps.setInt(3, lineId.getInt(1));
                        ResultSet b = ps.executeQuery();
                        b.next();
                        int stop1 = b.getInt(1);
                        b.next();
                        int stop2 = b.getInt(1);

                        // If these stops are not inside the reservation (between first and second)
                        if(!((stop1 <= first && stop1 <= second && stop2 <= first && stop2 <= second)
                                || stop1 >= first && stop1 >= second && stop2 >= first && stop2 >= second)){
                        
                            // Seats that are not available are removed from this array
                            System.out.println("Seat n°" + seats.getInt(1) + " is already reserved.");
                            available.set(seats.getInt(1) - carType.getInt("num_seat_min"), Boolean.FALSE);
                        }
                    }
                }
                
                nbOfAvailableSeats = 0;
                int a = 0;
                for (Iterator<Boolean> iterator = available.iterator(); iterator.hasNext();) {
                    Boolean next = iterator.next();
                    System.out.print(next + " ");
                    // If seat is available
                    if(next){
                        // Counting available seats
                        nbOfAvailableSeats++;
                        
                        // If we still need seats, we take it
                        if(passengerCount > 0){
                            seatsList.add(new Seat(cars.getInt(1), a + carType.getInt("num_seat_min")));
                            System.out.println("=> You took car " + cars.getInt(1) + " and seat " + (a + carType.getInt("num_seat_min")));
                            passengerCount--;
                        }
                    }
                    a++;
                }
                System.out.println("");
            }
            else{
                System.out.println("Class not matching.");
            }
            
            System.out.println("The car has " + nbOfAvailableSeats + "/" + cars.getInt("number_of_seats") + " seats left.");
            System.out.println("");
        }
        
        
        // The loop for cars is finished, now we can check if we have found enough available seats
        int i = 0;
        for (Iterator<Seat> iterator = seatsList.iterator(); iterator.hasNext();) {
            Seat next = iterator.next();
            System.out.println("===============================================");
            System.err.println(next.getCarNumber() + " " + next.getSeatNumber());
            i++;
        }
        
        // NOW WE NEED TO COMPUTE THE TOTAL PRICE

        Boolean countKilometers = false;
        int km = 0;
        int prevCity = -1;
        while(stops.next()){
            if(countKilometers){
                // Find the distance between this a city and its previous city
                ps = connection.prepareStatement("SELECT distance FROM cityLinks WHERE (city_id_one = ? AND city_id_two = ?) OR (city_id_one = ? AND city_id_two = ?)");
                ps.setInt(1, prevCity);
                ps.setInt(2, stops.getInt(2));
                ps.setInt(3, stops.getInt(2));
                ps.setInt(4, prevCity);
                ResultSet cityLinks = ps.executeQuery();
                cityLinks.next();
                
                // We increment the total distance
                km += cityLinks.getInt(1);
                
                System.out.println("Distance between " + prevCity + " and " + stops.getInt(2) + ": " + cityLinks.getInt(1));
            }
            
            System.out.println("Stop " + stops.getInt(2) + " is city " + stops.getInt(1));
            
            // If it is departure or arrival, we start or stop counting
            if(stops.getInt(1) == cityId1 || stops.getInt(1) == cityId2){
                if(!countKilometers){
                    // Start
                    countKilometers = true;
                }
                else{
                    // Stop
                    countKilometers = false;
                }
            }
            
            
            prevCity = -1;
            if(countKilometers){
                // Between this city id and the next one
                prevCity = stops.getInt(1);
            }
        }
        
        // Total distance
        System.out.println("");
        System.out.println("The total distance for your trip is " + km);
        System.out.println("");
        
        // Calculating the class coefficient
        int classNumber = (travelClass == Class.FIRST) ? 1 : 2;
        ps = connection.prepareStatement("SELECT kilometer_price FROM class WHERE id = ?");
        ps.setInt(1, classNumber);
        ResultSet kilometer_price = ps.executeQuery();
        kilometer_price.next();
        float km_price = kilometer_price.getFloat(1);
        
        if(passengerCount > 0){
            System.out.println("Not enough seats available in this train");
            System.out.println("missing seats: " + passengerCount);
        }
        else{
            
            
        

            /*

            INSERT SEAT
            CREATE RANDOM STRING 


            */
            
            
            boolean alreadyTaken;
            Random randomGenerator = new Random();
            String bookingId;
                    
            do{
                bookingId = "";
                
                for(int j = 0; j < 6; j++){
                    char c = 'A';
                    c += randomGenerator.nextInt(25);
                    bookingId += c;
                }
                
                ps = connection.prepareStatement("SELECT COUNT(*) FROM bookings WHERE booking_id = ?");
                ps.setString(1,bookingId);
                ResultSet findBookingId = ps.executeQuery();
                findBookingId.next();
                
                if(findBookingId.getInt(1) > 0)
                    alreadyTaken = true;
                else
                    alreadyTaken = false;
                
            }while(alreadyTaken);
            
            for (Iterator<Seat> iterator = seatsList.iterator(); iterator.hasNext();) {
                Seat next = iterator.next();
                ps = connection.prepareStatement("INSERT INTO seats (booking_id,car_number,rid,seat_number,departure_city_id,arrival_city_id,seat_price) VALUES (?,?,?,?,?,?,?)");
                ps.setString(1,bookingId);
                ps.setInt(2,next.getCarNumber());
                ps.setInt(3,ride_id.getInt(1));
                ps.setInt(4,next.getSeatNumber());
                ps.setInt(5,cityId1);
                ps.setInt(6,cityId2);
                ps.setInt(7,20);
                
                ps.executeUpdate();
                
                try{
                    connection.commit();
                }catch(SQLException se){
                    //Handle errors for JDBC
                    se.printStackTrace();
                    // If there is an error then rollback the changes.
                    System.out.println("Rolling back data here....");
                    connection.rollback();
                }
            }
            
            
            System.out.println("Your reservation is about to be completed...");
            System.out.println("");
            
            // Now computing the price
            amount = seatsList.size() * ((km * km_price) * discount + 20);
            
            System.out.println("Seat alone: " + 20);
            System.out.println("Cost for one seat on this trip: " + (20 + km * km_price));
            System.out.println("With the period discount coef(" + discount + "): " + (20 + km * km_price * discount));
            System.out.println("Number of people: " + seatsList.size());
            System.out.println("");
            System.out.println("Total cost of your trip is " + amount);
            
            // Sending the booking
            Booking b = new Booking(customerEmail, amount, departureDate, seatsList);
            ps = connection.prepareStatement("INSERT INTO bookings (booking_id,train_number,client_email,departure_date_time) VALUES (?,?,?,?)");
            ps.setString(1,bookingId);
            ps.setInt(2,trainNumber);
            ps.setString(3,customerEmail);
            ps.setDate(4,new java.sql.Date(departureDate.getTime()));
            ps.executeUpdate();
            try{
                connection.commit();
            }catch(SQLException se){
                //Handle errors for JDBC
                se.printStackTrace();
                // If there is an error then rollback the changes.
                System.out.println("Rolling back data here....");
                connection.rollback();
            }
            
            return b;
        }
        
        return null;
    }

    /**
     * See Operation 2.1.4
     *
     * @param bookingID
     * @param customerEmail
     *
     * @return <code>true</code> if the booking was cancelled, and
     * <code>false</code> otherwise
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public boolean cancelBooking(String bookingID, String customerEmail)
        throws DataAccessException, SQLException {
        
        Statement stmt = connection.createStatement();
        if(stmt.executeUpdate("DELETE FROM bookings where booking_id = '" + bookingID + "' and client_email = '" + customerEmail +"'") > 0){
            stmt.executeUpdate("DELETE FROM seats where booking_id = '" + bookingID+"'");
            try{
                connection.commit();
            }catch(SQLException se){
                //Handle errors for JDBC
                se.printStackTrace();
                // If there is an error then rollback the changes.
                System.out.println("Rolling back data here....");
                connection.rollback();
            }
            return true;
        }
        return false;
    }

    /**
     * See Operation 2.2.2
     *
     * @param trainNumber
     * @param departureDate
     * @param beginStation
     * @param endStation
     *
     * @return the list of available seats, including the empty list if no seat
     * is available
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public List<Seat> getAvailableSeats(int trainNumber, java.util.Date departureDate, String beginStation, String endStation)
        throws DataAccessException, SQLException {
        
        // Various declarations
        PreparedStatement ps = null;
        int nbOfAvailableSeats = 0;
        List<Seat> seatsList = new ArrayList<Seat>();
        
        // Obtaining period color with departure date
        ps = connection.prepareStatement("SELECT period_color FROM calendar WHERE begin_date <= ? AND end_date >= ? ");
        ps.setDate(1, new java.sql.Date(departureDate.getTime()));
        ps.setDate(2, new java.sql.Date(departureDate.getTime()));
        ResultSet period_color = ps.executeQuery();
        period_color.next();
        System.out.println(period_color.getString(1));
        
        
        // Obtaining ride id with period color and train number
        ps = connection.prepareStatement("SELECT rid FROM rides WHERE train_number = " + trainNumber + " AND period_color = '" + period_color.getString(1) + "'");
        ResultSet ride_id = ps.executeQuery();
        ride_id.next();
        System.out.println("Ride " + ride_id.getInt(1) + " is in train number " + trainNumber + " on the " + period_color.getString(1) + " period.");
        System.out.println("");
        
        
        // For each car in a ride
        ps = connection.prepareStatement("SELECT * FROM cars WHERE rid = " + ride_id.getInt(1));
        ResultSet cars = ps.executeQuery();
        while(cars.next()){
            System.out.println("The car " + cars.getInt("car_number") + " is being checked...");
            
            // Nb of available seats set to nb of seats
            nbOfAvailableSeats = cars.getInt("number_of_seats");
            
            ps = connection.prepareStatement("SELECT * FROM carTypes WHERE car_type = '" + cars.getString("car_type") + "'");
            ResultSet carType = ps.executeQuery();
            carType.next();
            
            System.out.println("The car class is " + carType.getInt("class") + ".");
            


            // Look for available seats in this car
            ps = connection.prepareStatement("SELECT seat_number FROM seats WHERE car_number = " + cars.getInt("car_number") + " AND rid = " + ride_id.getInt(1));
            ResultSet seats = ps.executeQuery();

            // Flags array for seats
            List<Boolean> available = new ArrayList<Boolean>();
            for(int i = carType.getInt("num_seat_min"); i < carType.getInt("num_seat_max") + 1; i++){
                available.add(Boolean.TRUE);
            }
            System.out.println("size of array is " + available.size());

            while(seats.next()){
                // Seats that are not available are removed from this array
                System.out.println("Seat n°" + seats.getInt(1) + " is already reserved.");
                available.set(seats.getInt(1) - carType.getInt("num_seat_min"), Boolean.FALSE);
            }

            nbOfAvailableSeats = 0;
            int a = 0;
            for (Iterator<Boolean> iterator = available.iterator(); iterator.hasNext();) {
                Boolean next = iterator.next();
                System.out.print(next + " ");
                // If seat is available
                if(next){
                    // Counting available seats
                    nbOfAvailableSeats++;
                    
                    seatsList.add(new Seat(cars.getInt(1), a + carType.getInt("num_seat_min")));
                    //System.out.println("=> Car " + cars.getInt(1) + " and seat " + (a + carType.getInt("num_seat_min")));
                    System.out.println(cars.getInt(1) + ":" + (a + carType.getInt("num_seat_min")));
                }
                a++;
            }
            System.out.println("");
            
            System.out.println("The car has " + nbOfAvailableSeats + "/" + cars.getInt("number_of_seats") + " seats left.");
            System.out.println("");
        }
        
        return seatsList;
    }

    /**
     * Closes the underlying connection and releases all related ressources. The
     * application must call this method when it is done accessing the data
     * store.
     *
     * @throws DataAccessException if an unrecoverable error occurs
     */
    public void close() throws DataAccessException, SQLException {
        if(connection!=null){
            
            connection.close();
        }
    }

}
